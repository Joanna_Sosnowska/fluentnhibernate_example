﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Models
{
    public class Person
    {
        private int id;
        private string name;
        private string phonenumber;
        private string email;
        public Person()
        {

        }
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Email { get; set; }
    }
}
