﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Models
{
    public class Department
    {
        private int id;
        private string name;
        public Manager manager;
        public Department()
        {
        }
        public virtual int Id { get; set; }
        public virtual Manager Manager { get; set; }
        public virtual string Name { get; set; }
    }
}
