﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Models
{
    public class Employee : Person
    {
        private int employeenumber;
        private DateTime dob;
        public List<Project> projects;
        public Employee()
        {

            this.Projects = new List<Project> { };
        }
        public virtual List<Project> Projects { get; set; }
        public virtual int EmployeeNumber { get; set; }
        public virtual DateTime BirthDate { get; set; }
    }
}
