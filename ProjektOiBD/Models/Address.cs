﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Models
{
    public class Address
    {
        public int id;
        public Customer customer;
        private string street;
        private string city;
        private string country;
        public ZipCode zipcode;
        public Address()
        {

        }
        public virtual int Id { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual string Street { get; set; }
        public virtual string City { get; set; }
        public virtual string Country { get; set; }
        public virtual ZipCode ZipCode { get; set; }
    }
}
