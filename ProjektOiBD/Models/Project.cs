﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Models
{
    public class Project
    {
        private int id;
        private string name;
        private string costcode;
        public List<Employee> employees;
        public Project()
        {
            this.Employees = new List<Employee> { };
        }
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string CostCode { get; set; }
        public virtual List<Employee> Employees { get; set; }
        public void AssignEmployee(Employee emp)
        {
            Employees.Add(emp);
        }
    }
}
