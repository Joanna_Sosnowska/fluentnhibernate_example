﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Models
{
    public class Manager : Employee
    {
        private LinkedList<Employee> employees;
        public Manager()
        {
            this.Employees = new LinkedList<Employee> { };
        }
        public virtual LinkedList<Employee> Employees { get; set; }
        public void AddEmployee(Employee emp)
        {
            Employees.AddLast(emp);
        }
    }
}
