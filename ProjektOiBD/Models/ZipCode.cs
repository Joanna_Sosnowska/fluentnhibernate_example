﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Models
{
    public class ZipCode
    {
        private int id;
        private string state;
        private string code;
        private string extension;
        public Address adres;
        public ZipCode()
        {

        }
        public virtual int Id { get; set; }
        public virtual string State { get; set; }
        public virtual string Code { get; set; }
        public virtual string Extension { get; set; }
        public virtual Address Adres { get; set; }
    }
}
