﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Models
{
    public class CasualEmployee : Employee
    {
        private double[] timerecord;
        private static int MAXTIMERECORDS = 10;
        private int numberoftimerecords;
        public CasualEmployee()
        {
            numberoftimerecords = 100;
            timerecord = new double[numberoftimerecords];
            for (int i = 0; i < timerecord.Length; i++)
            {
                timerecord[i] = 0;
            }

        }
        public double[] TimeRecord { get; set; }
        public virtual double NumberOfTimeRecords { get; set; }
        public void AddTimeRecord(double t)
        {
            for (int i = 0; i < this.TimeRecord.Length; i++)
            {
                if (TimeRecord[i] == 0)
                {
                    TimeRecord[i] = t;
                    break;
                }
            }
        }

    }
}
