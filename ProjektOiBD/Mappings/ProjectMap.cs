﻿using FluentNHibernate.Mapping;
using ProjektOiBD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Mappings
{
    public class ProjectMap : ClassMap<Project>
    {
        public ProjectMap()
        {
            Schema("MyDb");
            Table("Projects");
            Id(p => p.Id)
              .Column("ProjectID")
              .CustomType("Int32")
              .Access.Property()
              .CustomSqlType("int")
              .Not.Nullable()
              .Precision(10)
              .GeneratedBy.Identity();
            Map(p => p.Name)
                .Column("Name")
                .CustomType("String")
                .Access.Property()
                .Generated.Never()
                .CustomSqlType("nvarchar")
                .Not.Nullable()
                .Length(100);
            Map(p => p.CostCode)
                .Column("CostCode")
                .CustomType("String")
                .Access.Property()
                .Generated.Never()
                .CustomSqlType("nvarchar")
                .Not.Nullable()
                .Length(30);
            HasManyToMany(p => p.Employees)
                .ParentKeyColumn("ProjectID")
                .ChildKeyColumn("ID")
                .Cascade.All()
                .Table("ProjectMembers");
        }
    }
}
