﻿using FluentNHibernate.Mapping;
using ProjektOiBD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Mappings
{
    public class CustomerMap : SubclassMap<Customer>
    {
        public CustomerMap()
        {
            Schema("MyDb");
            Table("Customers");

            KeyColumn("ID");
            HasOne(p => p.Adres)
               .Cascade.All();
        }
    }
}
