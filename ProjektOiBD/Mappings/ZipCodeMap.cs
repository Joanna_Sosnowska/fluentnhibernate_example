﻿using FluentNHibernate.Mapping;
using ProjektOiBD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Mappings
{
    public class ZipCodeMap : ClassMap<ZipCode>
    {
        public ZipCodeMap()
        {
            Schema("MyDb");
            Table("ZipCodes");
            Id(z => z.Id).GeneratedBy.Foreign("Address");

            Map(z => z.State)
              .Column("State")
              .CustomType("String")
              .Access.Property()
              .Generated.Never()
              .CustomSqlType("nvarchar")
              .Not.Nullable()
              .Length(100);
            Map(z => z.Code)
              .Column("Code")
              .CustomType("String")
              .Access.Property()
              .Generated.Never()
              .CustomSqlType("nvarchar")
              .Not.Nullable()
              .Length(50);
            Map(z => z.Extension)
              .Column("Extension")
              .CustomType("String")
              .Access.Property()
              .Generated.Never()
              .CustomSqlType("nvarchar")
              .Not.Nullable()
              .Length(100);
            References(z => z.Adres)
                .Cascade.All()
                .Unique();
        }
    }
}
