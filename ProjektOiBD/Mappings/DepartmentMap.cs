﻿using FluentNHibernate.Mapping;
using ProjektOiBD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Mappings
{
    public class DepartmentMap : ClassMap<Department>
    {
        public DepartmentMap()
        {
            Schema("MyDb");
            Table("Departments");
            Id(d => d.Id)
              .Column("IdNumber")
              .CustomType("Int32")
              .Access.Property()
              .CustomSqlType("int")
              .Not.Nullable()
              .Precision(10)
              .GeneratedBy.Identity();
            Map(d => d.Name)
                .Column("Name")
                .CustomType("String")
                .Access.Property()
                .Generated.Never()
                .CustomSqlType("nvarchar")
                .Not.Nullable()
                .Length(100);
            References(d => d.Manager).Column("Name");
        }
    }
}
