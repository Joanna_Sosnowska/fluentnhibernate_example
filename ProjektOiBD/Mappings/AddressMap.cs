﻿using FluentNHibernate.Mapping;
using ProjektOiBD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Mappings
{
    public class AddressMap : ClassMap<Address>
    {
        public AddressMap()
        {
            Schema("MyDb");
            Table("Addresses");
            Id(a => a.Id).GeneratedBy.Foreign("Customer");
            References(a => a.Customer)
                .Unique()
                .Cascade.All();
            Map(a => a.Street)
              .Column("Street")
                .CustomType("String")
                .Access.Property()
                .Generated.Never()
                .CustomSqlType("nvarchar")
                .Not.Nullable()
                .Length(100);
            Map(a => a.City)
              .Column("City")
                .CustomType("String")
                .Access.Property()
                .Generated.Never()
                .CustomSqlType("nvarchar")
                .Not.Nullable()
                .Length(100);
            Map(a => a.Country)
                .Column("Country")
                .CustomType("String")
                .Access.Property()
                .Generated.Never()
                .CustomSqlType("nvarchar")
                .Not.Nullable()
                .Length(100)
                .Not.Nullable();
            HasOne(a => a.ZipCode)
               .Cascade.All();
        }
    }
}
