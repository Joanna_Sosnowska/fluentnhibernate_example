﻿using FluentNHibernate.Mapping;
using ProjektOiBD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Mappings
{
    public class CasualEmployeeMap : SubclassMap<CasualEmployee>
    {
        public CasualEmployeeMap()
        {
            Schema("MyDb");
            Table("Workers");

            KeyColumn("ID");
            Map(p => p.NumberOfTimeRecords)
               .Column("NumberOfTimeRecord")
               .CustomType("Int32")
               .Access.Property()
               .Generated.Never()
               .CustomSqlType("int")
               .Not.Nullable()
               .Precision(5);
        }
    }
}
