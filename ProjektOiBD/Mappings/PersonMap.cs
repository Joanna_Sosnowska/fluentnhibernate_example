﻿using FluentNHibernate.Mapping;
using ProjektOiBD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Mappings
{
    public class PersonMap : ClassMap<Person>
    {
        public PersonMap()
        {
            Schema("MyDb");
            Table("People");
            Id(p => p.Id)
              .Column("ID")
              .CustomType("Int32")
              .Access.Property()
              .CustomSqlType("int")
              .Not.Nullable()
              .Precision(10)
              .GeneratedBy.Identity();
            Map(p => p.Name)
                .Column("Name")
                .CustomType("String")
                .Access.Property()
                .Generated.Never()
                .CustomSqlType("nvarchar")
                .Not.Nullable()
                .Length(100);
            Map(p => p.PhoneNumber)
                .Column("Phone")
                .CustomType("String")
                .Access.Property()
                .Generated.Never()
                .CustomSqlType("nvarchar")
                .Not.Nullable()
                .Length(100);
            Map(p => p.Email)
                .Column("Email")
                .CustomType("String")
                .Access.Property()
                .Generated.Never()
                .CustomSqlType("nvarchar")
                .Not.Nullable()
                .Length(100);
        }
    }
}
