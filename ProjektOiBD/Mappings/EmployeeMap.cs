﻿using FluentNHibernate.Mapping;
using ProjektOiBD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Mappings
{
    public class EmployeeMap : SubclassMap<Employee>
    {
        public EmployeeMap()
        {
            Schema("MyDb");
            Table("Employees");

            KeyColumn("ID");
            Map(p => p.EmployeeNumber)
                .Column("EmpNumber")
                .CustomType("Int32")
                .Access.Property()
                .Generated.Never()
                .CustomSqlType("int")
                .Not.Nullable()
                .Precision(5);
            Map(p => p.BirthDate)
                .CustomType("datetime2")
                .Not.Nullable()
                .Access.Property()
                .Generated.Never();
            HasManyToMany(p => p.Projects)
                .ParentKeyColumn("ID")
                .ChildKeyColumn("ProjectID")
                .Cascade.All()
                .Table("ProjectMembers");

        }
    }
}
