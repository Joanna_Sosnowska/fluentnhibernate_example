﻿using FluentNHibernate.Mapping;
using ProjektOiBD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektOiBD.Mappings
{
    public class ManagerMap : SubclassMap<Manager>
    {
        public ManagerMap()
        {
            Schema("MyDb");
            Table("Managers");

            KeyColumn("ID");
            HasMany(p => p.Employees)
                .KeyColumn("ID")
                .Table("Employees")
                .Cascade.All();

        }
    }
}
