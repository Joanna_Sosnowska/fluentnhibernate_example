﻿using ProjektOiBD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektOiBD
{
    public class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                NHibernateHelper.InitializeSessionFactory();
                using (var session = NHibernateHelper.SessionFactory.OpenSession())
                using (var tx = session.BeginTransaction())
                {
                    Employee emp1 = new Employee();
                    emp1.Name = "Jan Kowalski";
                    emp1.PhoneNumber = "62133444";
                    emp1.Email = "jkowalski@wp.pl";
                    emp1.BirthDate = new DateTime(1988, 10, 12);
                    emp1.EmployeeNumber = 1;

                    Project p1 = new Project();
                    p1.Name = "Projekt1";
                    p1.CostCode = "000a";
                    p1.AssignEmployee(emp1);
                    session.Save(emp1);
                    session.Save(p1);

                    tx.Commit();
                }
                Main MainWindow = new Main();
                Application.Run(MainWindow);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.InnerException.InnerException.ToString());

            }
        }
    }
}